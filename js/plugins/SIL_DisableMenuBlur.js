﻿/*:
-------------------------------------------------------------------------
@title Disable Menu Blur
@author SilicaAndPina
@version 1.0
@date 21 Nov 2017
@filename SIL_DisableMenuBlur.js
@url http://silicagames.ml

Disables the blur effect in the load, options, and main menu screens 

-------------------------------------------------------------------------------
@plugindesc Disables the blur effect in the load, options, and main menu screens 
@help 
-------------------------------------------------------------------------------
== Description ==
Disables the blur effect in the load, options, and main menu screens 
Free for commercial and non commercial use.
no credit required.
-------------------------------------------------------------------------------
 */ 

SceneManager.snapForBackground = function() {
    this._backgroundBitmap = this.snap();
    this._backgroundBitmap.opacity = 0;
};
