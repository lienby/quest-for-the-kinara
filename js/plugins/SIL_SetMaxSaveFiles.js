﻿/*:
-------------------------------------------------------------------------
@title Disable Menu Blur
@author SilicaAndPina
@version 1.0
@date 21 Nov 2017
@filename SIL_SetMaxSaveFiles.js
@url http://silicagames.ml

@param MaxSaveFiles
@type number
@min 1
@desc Limit the number of save files 
(Default: 20)
@default 20

Change the number of maximum save files

-------------------------------------------------------------------------------
@plugindesc Change the number of maximum save files
@help 
-------------------------------------------------------------------------------
== Description ==

Change the maximum number of maximum save files
Free for commercial and non commercial use.
no credit required.
-------------------------------------------------------------------------------
 */ 
save = PluginManager.parameters('SIL_SetMaxSaveFiles');
save = save['MaxSaveFiles'];
DataManager.maxSavefiles = function() {
    return save;
};
